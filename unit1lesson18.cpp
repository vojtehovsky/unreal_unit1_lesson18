// unit1lesson18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

using namespace std;

template <class T> class Stack {
private:
    T* m_array;
    int m_count;
    int m_max_size;
    static const int m_growth_factor = 2;
    static const int m_initial_max_size = 10;
    void init();
    void increase_array_size();
public:
    Stack();
    ~Stack();
    void push(T k);
    T pop();
    bool isEmpty();

};

template <class T> Stack<T>::Stack() : m_count(0), m_max_size(m_initial_max_size)
{
    init();
}

template <class T> Stack<T>::~Stack()
{
    delete[] m_array;
}

template <class T> void Stack<T>::init()
{
    m_array = new T[m_max_size];
    m_count = 0;
}

template <class T> void Stack<T>::increase_array_size()
{
    m_max_size = m_growth_factor * m_max_size;
    T* tmp = new T[m_max_size];

    for (int i = 0; i < m_count; i++)
        tmp[i] = m_array[i];

    delete[] m_array;

    m_array = tmp;
}

template <class T> void Stack<T>::push(T data)
{
    if (m_count == m_max_size)
        increase_array_size();
    m_array[m_count++] = data;
}


template <class T> T Stack<T>::pop()
{
    if (m_count == 0)
        throw std::underflow_error("Underflow Exception!!!");
    T popped_element = m_array[--m_count];
    return popped_element;
}

template <class T> bool Stack<T>::isEmpty()
{
    if (m_count == 0)
        return 1;
    else
        return 0;
}



int main()
{
    Stack<string> stack;

    stack.push("elem 1");
    stack.push("elem 2");
    stack.push("elem 3");


    cout << "poping element: " << stack.pop() << endl;
    cout << "poping element: " << stack.pop() << endl;
    cout << "is stack empty: " << stack.isEmpty() << endl;
    cout << "poping element: " << stack.pop() << endl;
    cout << "is stack empty: " << stack.isEmpty() << endl;


    return 0;
}
